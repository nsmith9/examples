// memoryleak.cpp: where is my mind

#include <cstdlib>
#include <ctime>
#include <algorithm>
#include <iostream>
#include <vector>

const int NITEMS = 1<<10;
const int TRIALS = 100;

using namespace std;

bool duplicates(int n) {
    int *randoms = new int[n];

    for (int i = 0; i < NITEMS; i++) {
    	randoms[i] = rand(); // Add % 1000 to trigger memory leak
    }

    for (int i = 0; i < n; i++) {
	if (find(randoms + i + 1, randoms + NITEMS, randoms[i]) != (randoms + NITEMS)) {
	    return true;
	}
    }

    delete []randoms;
    return false;
}

int main(int argc, char *argv[]) {
    srand(time(NULL));

    for (int i = 0; i < TRIALS; i++) {
    	if (duplicates(NITEMS)) {
	    cout << "Duplicates Detected!" << endl;
	}
    }

    return 0;
}
