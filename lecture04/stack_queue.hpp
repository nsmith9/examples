#pragma once

#include <stack>

template <typename T>
class stack_queue {
    std::stack<T> in, out;

    void in_to_out() {
    	while (!in.empty()) {
    	    out.push(in.top());
    	    in.pop();
	}
    }

    public:
	bool empty()     const { return in.empty() && out.empty(); }
	const T& front() {
	    if (out.empty()) {
	    	in_to_out();
	    }
	    return out.top();
	}
	const T& back()  const { return in.top(); }
	void push(const T& value) {
	    in.push(value);
	}
	void pop() {
	    if (out.empty()) {
	    	in_to_out();
	    }
	    out.pop();
	}
};
